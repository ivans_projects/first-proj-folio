var regexEmail = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
var nameField  = document.getElementsByClassName('toch__item--text')[0];
var emailField = document.getElementsByClassName('toch__item--email')[0];
var textArea   = document.getElementsByClassName('toch__item--textarea')[0];

function validate(){
    if( (nameField.value) )
         {
            nameField.classList.remove('error');
            document.contactForm.Name.focus() ;
         } else {
             nameField.classList.add('error');
             document.contactForm.Name.focus() ;
         }
    if( regexEmail.test(emailField.value) )
        {
            emailField.classList.remove('error');
            document.contactForm.Email.focus() ;
        } else {
            emailField.classList.add('error');
            document.contactForm.Email.focus() ;
        }
    if((textArea.value)){
        textArea.classList.remove('error');
    }
    else {
        textArea.classList.add('error');
    }
}
///
$(document).ready(function() {
    var text_max = 250;
    $('#textarea_feedback').html(text_max + ' pozostało znaków');
    $('.toch__item--textarea').keyup(function() {
        var text_length = $('.toch__item--textarea').val().length;
        var text_remaining = text_max - text_length;
        $('#textarea_feedback').html(text_remaining + ' pozostało znaków');
    });
});
///
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    responsive:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
